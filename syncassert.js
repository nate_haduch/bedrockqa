var assert = require('assert');
var fs = require('fs');  
var UserId = process.argv[2]
var fileName = UserId + ".rtf"
var startString = "event emitter init for cust " + UserId
var testStrings = ["CACHE CREATE","CACHE UPDATE","REMOTE CREATE"]
var dataStrings = ["DATA","","REMOTE DATA"]
var humanStrings = [" records created", " records updated"," remote records created"]

fs.exists(fileName, function(exists) {
  if (exists) {
		var contents = fs.readFileSync(fileName,"utf8");
		var n = contents.lastIndexOf(startString);
		var lastSyncString = contents.substr(n);
		testAll(lastSyncString);
		printIndexEntries(lastSyncString);
		}
  else {
  		console.log("Could not file " + fileName)
  		}
	})

// function that actually does the parsing
function testAll(str) {
	for (var i=0;i<testStrings.length;i++) {
		if (str.search("ERROR") == -1) {

			var re = new RegExp(testStrings[i], 'gi');
			var updates = str.match(re);


			if (updates)  {
				var numUpdates = updates.length
				if (i == 0) {
					var system = str.match(/DATA\s(\w*)\//)
					console.log(numUpdates + humanStrings[i] + " on system " + system[1])

				}
				else if (i == 2) {
					var system = str.match(/REMOTE DATA\s(\w*)\//)
					console.log(numUpdates + humanStrings[i] + " on system " + system[1])
				}
				else {
					console.log(numUpdates + humanStrings[i])
				}
				
			}
			else	{
				console.log("No" + humanStrings[i])

			}

		}
	}
}		

function printIndexEntries(str) {
	var eachIndex = str.match(/INDEX\s(.*)/g)
	for (i=0;i<eachIndex.length;i++) {
		console.log("Indexed " + eachIndex[i])
}
}