import time
import unittest
import sys
import accountFunctions
import connectorFunctions

from selenium import webdriver
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

username = "nate@bedrockdata.com"
ConnectorList = [sys.argv[2]]
DevNum = sys.argv[1]
#AddList = ["ConnectWise"]

# start chrome
driver = webdriver.Chrome()
URL = "http://bedrock.dev:8800/"
URL = "https://my.qa0.bedrockdata.com"
driver.get(URL)

accountFunctions.Login(driver,username)
accountFunctions.ChangeCustomer(driver,DevNum)


connectorFunctions.GoToConnectorsPage(driver)
#connectorFunctions.AddConnectors(driver,AddList)
connectorFunctions.InstallConnectors(driver,ConnectorList)
connectorFunctions.UninstallConnectors(driver)
