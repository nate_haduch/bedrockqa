import time
import unittest
import sys
import accountFunctions
import connectorFunctions
import mappingFunctions
import assertions

from selenium import webdriver
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

driver = webdriver.Chrome()
URL="https://accounts.qa1.bedrockdata.com/"
driver.get(URL)
InstallList = ["Batchbook","Capsule CRM"]

accountFunctions.CreateAccount(driver,"389")

connectorFunctions.InstallConnectors(driver,InstallList)

mappingFunctions.AutoGenerateMappingsFromModal(driver)

accountFunctions.DashboardSubscribe(driver)

assertions.SubscribedOrNot(driver,"Chrome")
