import time
import unittest
import sys
import accountFunctions

from selenium import webdriver
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

#URL = "https://my.qa1.bedrockdata.com"
URL = "http://bedrock.dev:8800/"


# start chrome
driver = webdriver.Chrome()
driver.get(URL)

accountFunctions.CreateAccount(driver,sys.argv[1])

# accountFunctions.Login(driver,credU,credP)
# accountFunctions.ChangeCustomer(driver,sys.argv[1])
# accountFunctions.SyncToggle(driver)