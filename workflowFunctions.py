import time
import unittest
import sys
import assertions

from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException

def GoToWorkflowPage(driver):	
	workflowPage = WebDriverWait(driver, 10).until(
		EC.visibility_of_element_located((By.CLASS_NAME, "fa-sitemap"))
	)
	workflowPage.click()

	try:
		EditButton = driver.find_element_by_class_name("intro-btn").click()
	except NoSuchElementException:
		pass

def AddWorkFlow(driver):
	driver.find_element_by_class_name("pull-right").click()
	driver.find_element_by_name("actions[sync][enabled]").click()
	driver.find_element_by_class_name("btn-primary").click()
