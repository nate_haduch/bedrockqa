import time
import unittest
import sys
import datetime
import assertions

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import NoSuchElementException


def Login(driver,credU,credP="brd$123"):
	
	username = WebDriverWait(driver, 10).until(
		EC.visibility_of_element_located((By.NAME, "username"))
	)

	username.send_keys(credU)
	driver.find_element_by_name('password').send_keys(credP)
	driver.find_element_by_class_name('btn-login').click()

def Logout(driver):
	driver.find_element_by_class_name("dropdown-toggle").click()
	driver.find_element_by_link_text("Logout").click()

# I made this so you can just try it and it'll create teh account if it doesn't exist
def ChangeCustomer(driver,CustomerID):
	
	DevCustomerID = CustomerID

	dropdown = WebDriverWait(driver, 10).until(
		EC.visibility_of_element_located((By.CLASS_NAME, "dropdown-toggle"))
	)
	dropdown.click()

	#	driver.find_element_by_class_name('fa-cog').click()
	driver.find_element_by_link_text('Customers').click()

	search = WebDriverWait(driver, 10).until(
		EC.visibility_of_element_located((By.NAME, "search"))
	)
	search.send_keys(DevCustomerID)
	search.send_keys(Keys.RETURN)

	# filtering takes a second
	time.sleep(1)
	
	# Access the customer at the name above (really this is just finding the first one)
	try: 
		driver.find_element_by_class_name('btn-access').click()
	except NoSuchElementException:
		driver.find_element_by_link_text("Sign Out").click()
		CreateAccount(driver,CustomerID)

# shouldn't really need to invoke directly too often
def CreateAccount(driver, DevNum, QAPass="brd$123"):

	# constants to input
	QAName = "dev-" + str(DevNum) + "-qa" + "-" + str(datetime.datetime.now().time())[-6:]
	emailAddress = "nate+" + QAName + "@bedrockdata.com"

	SignUp = WebDriverWait(driver, 10).until(
		EC.visibility_of_element_located((By.LINK_TEXT, "Sign Up Now"))
	)
	SignUp.click()

	# when the page loads, fill in the fields
	email = WebDriverWait(driver, 10).until(
		EC.visibility_of_element_located((By.NAME, "email"))
	)
	email.send_keys(emailAddress)

	driver.find_element_by_name('password').send_keys(QAPass)
	driver.find_element_by_name('username').click()

	driver.find_element_by_name('firstName').send_keys("Nate")
	driver.find_element_by_name('lastName').send_keys(QAName)
	driver.find_element_by_name('company').send_keys(QAName)
	driver.find_element_by_name('phone').send_keys("123")

	driver.find_element_by_class_name('btn-signup').click()

	assertions.AccountCreated(driver,emailAddress)

	Logout(driver)
	Login(driver,"nate@bedrockdata.com")
	ChangeCustomer(driver,DevNum)

def DashboardSubscribe(driver):

	dashButton = WebDriverWait(driver, 10).until(
		EC.visibility_of_element_located((By.CLASS_NAME, "fa-dashboard"))
	)
	dashButton.click()

	SubscribeButton = WebDriverWait(driver, 10).until(
		EC.visibility_of_element_located((By.LINK_TEXT, "SUBSCRIBE NOW"))
	)
	SubscribeButton.click()

	Subscribe(driver)	

def SyncToggle(driver):
	dashButton = WebDriverWait(driver, 10).until(
		EC.visibility_of_element_located((By.CLASS_NAME, "fa-dashboard"))
	)
	dashButton.click()

	driver.find_elements_by_tag_name('label')[0].click()

	time.sleep(2)
	
	driver.find_elements_by_tag_name('button')[2].click()

	time.sleep(1)

def GeneralSubscribe(driver):

	dropdown = WebDriverWait(driver, 10).until(
		EC.visibility_of_element_located((By.CLASS_NAME, "dropdown-toggle"))
	)
	dropdown.click()
	driver.find_element_by_class_name("fa-cog").click()

	billingInfo = WebDriverWait(driver, 10).until(
		EC.visibility_of_element_located((By.LINK_TEXT, "Billing Info"))
	)
	billingInfo.click()

	Subscribe(driver)

#has a user already logged in
def Subscribe(driver):

	firstName = WebDriverWait(driver, 10).until(
		EC.visibility_of_element_located((By.NAME, "first_name"))
	)
	firstName.send_keys("Nate")
	driver.find_element_by_name("last_name").send_keys("Haduch")
	driver.find_element_by_name("card_number").send_keys("4111-1111-1111-1111")
	driver.find_element_by_name("security_code").send_keys("354")
	driver.find_elements_by_tag_name('input')[4].click()
	driver.find_elements_by_tag_name('input')[4].send_keys("D")
	driver.find_elements_by_tag_name('input')[4].send_keys(Keys.TAB)
	driver.find_elements_by_tag_name('input')[5].click()
	driver.find_elements_by_tag_name('input')[5].send_keys("2020")
	driver.find_elements_by_tag_name('input')[5].send_keys(Keys.TAB)

	driver.find_element_by_name("address").send_keys("186 South St #500")
	driver.find_element_by_name("city").send_keys("Boston")
	driver.find_elements_by_tag_name('input')[10].click()
	driver.find_elements_by_tag_name('input')[10].send_keys("mass")
	driver.find_elements_by_tag_name('input')[10].send_keys(Keys.TAB)
	driver.find_element_by_name("zip_code").send_keys("02111")
	driver.find_element_by_name("terms").click()

	driver.find_element_by_class_name("btn-success").click()

def GoToDashboard(driver):
	dashButton = WebDriverWait(driver, 10).until(
		EC.visibility_of_element_located((By.CLASS_NAME, "fa-dashboard"))
	)