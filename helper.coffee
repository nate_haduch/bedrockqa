#helper function coffeescript
#Handlebars.registerHelper "markdown"

standardizeSettings = (str) ->
	switch str
	   when 'api-key' then return 'key'
	   when 'access-key' then return 'key' 
	   when 'api-token' then return 'key'
	   when 'auth-token' then return 'key'
	   when 'user-key' then return 'key'
	   when 'token' then return 'key'
	   when 'url' then return 'url'
	   when 'api-url' then return 'url'
	   when 'subdomain' then return 'url'
	   when 'site' then return 'url'
	   when 'sandbox' then return 'url'
	   when 'api-password' then return 'password'
	   when 'user' then return 'username'
	   when 'email-address' then return 'email'
	   when 'api-email' then return 'email'
	   when 'account-name' then return 'account'
	   when 'account' then return 'account'
	   else return str

console.log standardizeSettings(process.argv[2])