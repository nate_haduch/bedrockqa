import time
import unittest
import sys
import assertions

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import NoSuchElementException

def InstallConnectors(driver,InstallList,start=0):

	#ensure that we're alpha sorted
	InstallList.sort()

	# for some reason it's not happy unless I wait a second
	time.sleep(2)

	total = 0
	ConnectorNameList = driver.find_elements_by_tag_name("h4")
	InstallButtonList = driver.find_elements_by_class_name("btn-xs")

	for i in xrange(start, len(ConnectorNameList)-1):
		ConnectorName = ConnectorNameList[i].text
		if ((ConnectorName in InstallList) and (InstallButtonList[i].text == "INSTALL")):
			if ((ConnectorName == "iContact") or (ConnectorName == "Solve CRM") or (ConnectorName == "Work[etc]") or (ConnectorName == "Solve CRM")):
				assertions.ConnectorNotSupported(ConnectorName)
				InstallList.pop(0)
				if (len(InstallList) > 0):
					InstallConnectors(driver,InstallList,i+1)
				return 1


			# some IE catching
			InstallButtonList[i].location_once_scrolled_into_view

			# click on the install button
			InstallButtonList[i].click()

			# Salesforce.com is dumb and has two auth-links, so you can't locate by id
			if ((ConnectorName == "Salesforce.com") or (ConnectorName == "Veeva")):
				driver.find_element_by_class_name("auth-link").click()
				time.sleep(1)
				OffSiteAuth(driver,ConnectorName)
			elif (ConnectorName == "Microsoft Dynamics"):
				DynamicsAuth(driver)
			else:
				# Shopify requires a field filled in before external auth, which doesn't fit my pattern at all
				if (ConnectorName == "Shopify"):
					driver.find_element_by_id("user-store-name").send_keys("bedrocks-store")

				try: 
					driver.find_element_by_id("auth-link").click()

				except NoSuchElementException:
					FillInConnectorInfo(driver,ConnectorName)

				else:
					OffSiteAuth(driver,ConnectorName)

			# give it a couple seconds to finish up
			time.sleep(3)

			assertions.InstallSuccess(ConnectorName,InstallButtonList[i])

			# remove the first element, check if you're done
			InstallList.pop(0)
			if (len(InstallList) > 0):
				InstallConnectors(driver,InstallList,i+1)
			return 1


def UninstallConnectors(driver,invisible=0):
	
	UninstallButtonList = driver.find_elements_by_class_name("dropdown-toggle")
	for i in xrange(0, len(UninstallButtonList)-invisible):
		if (UninstallButtonList[i].text == "INSTALLED"):
			UninstallButtonList[i].click()
			driver.find_element_by_class_name('installed-agent').click()

			DropDownList = driver.find_elements_by_class_name('dropdown-toggle')
			DropDownList[len(DropDownList)-1].click()
			driver.find_element_by_link_text('Uninstall').click()

			time.sleep(1)

			# call this recursively because the page has changed
			UninstallConnectors(driver,invisible+1)	
			return 1 

#['key','url','account','username','email','password']
def FillInConnectorInfo(driver,ConnectorName):
	CredentialDictionary = {}
	CredentialDictionary['Base CRM'] = ["","","","","nate@bedrockdata.com","brd$123"]
	CredentialDictionary['Batchbook'] = ["KmVzY7XRHvSxG9bo7xLZ","","bedrockdatadev"]
	CredentialDictionary['Bigcommerce'] = [] # gonna be oauth
	CredentialDictionary['Capsule CRM'] = ["c5fab1daf7c0302bd2a8c97acf3293d6","https://bedrockdata.capsulecrm.com/"]
	CredentialDictionary['Close.io'] = ["348a1f77bf37752c8c4aa4b1cc0f0e29aae3c9d1a5dbfce3b42e1ff1"]
	CredentialDictionary['ConnectWise'] = ["v2015_6","staging.connectwisedev.com","bedrock_f","bedrock","","bedrock"]
	CredentialDictionary['Constant Contact'] = [] # Differs on my dev and on prod
	CredentialDictionary['Cvent'] = ["","https://sandbox-app.cvent.com/Subscribers/login.aspx?reset=97F81FF4-E669-40F5-ADD0-A30B29A3EB26","BRDSB001","BRDSB001Api","","BrdCvent737"] 
	CredentialDictionary['Datanyze'] = ["693a13134e4cbf8642b24953bc2c7193","","","","john@bedrockdata.com"]
	CredentialDictionary['Desk.com'] = ["","https://zzz-bedrockdatadev.desk.com/","","","devtools@bedrockdata.com","WxSfwIthBV11"]
	CredentialDictionary['Eloqua'] = ["","","","Ben.Smith","","Brd$1234"]
	CredentialDictionary['Freshdesk'] = ["afLC8l9oEKJjcBtdOdas","https://bedrockdata.freshdesk.com"]
	CredentialDictionary['Help Scout'] = ["5ebfc5ee3e04c61269b0bf5c79d21b58b553e315"]
	CredentialDictionary['Highrise'] = ["8838da20cbe451f93eeb25a936bbfc95","","bedrockdata1"]
	CredentialDictionary['HubSpot'] = ["36e819ea-1fbf-44bd-b4e3-7ef4cc26cedd"]
	CredentialDictionary['Insightly'] = ["f15117b5-8f0e-47de-8f5c-4fbda57589ed"]
	CredentialDictionary['MailChimp'] = ["b37510c33729786774ef00fc5fcfc946-us9"]
	CredentialDictionary['Marketo'] = [] # doesn't work ["7923494398493629552244558888FFAB11DD004D3335","https://481-NCR-692.mktoapi.com/soap/mktows/2_9","","bedrockdev1_82108619524588F91CFB53"]
	CredentialDictionary['Marketo Rest'] = ["hnBr48ScQ5c2sI7YE2QMIFvVLumsYohJ","https://481-NCR-692.mktorest.com/rest","","e2e2f3fc-66be-4bc5-b6c7-5db151a501e7"]
	CredentialDictionary['Mojo Help Desk'] = ["b20c5c5290d4d3122bebee231c8e1629f4e30321","http://bedrockdata.mojohelpdesk.com/login"]
	CredentialDictionary['NetSuite'] = ["","","","","taylor@bedrockdata.com","brd$1234567"]
	CredentialDictionary['Pardot'] = ["e4a7f46de88aa627d4937be74f5d8137","","","","ben@bedrockdata.com","SpiffyPants1!"] 
	CredentialDictionary['Pipedrive'] = ["9dc0652254c32c8e2f051f1ade95a8965a8c0476"]
	CredentialDictionary['PipelineDeals'] = ["O59k3rQVOKkqn4Oxcc"]
	CredentialDictionary['Recurly'] = ["11b670b0534543ae9cd4d0e43088ab37","bedrockdata.recurly.com"]
	CredentialDictionary['Repsly'] = ["","","","EAA21F1F-5B33-45C1-9C0E-403C181FA5CB","","9FA8F7A1-D86B-4A76-932E-2957FFC25AFC"]
	CredentialDictionary['Socious'] = [] # 502s for external auth http://bd.sociouscorp.com/l/li/in/ demobd@restapi.socious.com hP9Zc24q4Y4P
	CredentialDictionary['Stripe'] = ["https://my.bedrockdata.com/v2/hooks/collect/stripe0?id=5540ddc60688d36f6dbe6ef5&sig=3f334971899d5976cf65edf64478fe0d9591dcb2"]
	CredentialDictionary['Sugar CRM'] = ["admin","moose","http://crm.bedrockdata.com/sugar/index.php?action=Login&module=Users"] #api-user, api-pass, api-url
	CredentialDictionary['Vtiger'] = ["i6pSjnCyKxfODbGv","http://104.197.8.229/vtigercrm","","admin","","SpiffyPants1"]
	CredentialDictionary['Zendesk'] = ["U5WikAAxCVvu2qih0jUqF73VV6tqG1J1r4KtuVxS","https://bedrockdata.zendesk.com/","","","ben@bedrockdata.com"]
	CredentialDictionary['Zoho CRM'] = ["b2a48b04b044dda19e60d1a869a18322"]

	# these require a paid account to test with
	CredentialDictionary['iContact'] = [] # can't figure out urlapi - it looks like *Ben
	CredentialDictionary['Solve CRM'] = [] #"fcq7L0+d23sa6bJeu240nea5jbZ4Rdr4S257P5w1","","","nate@bedrockdata.com"]
	CredentialDictionary['WORK[etc]'] = [] # this one is tricky because they automatically remove your account if you don't enter payment info
	CredentialDictionary['Weclapp'] = [] # ["90adcde1-98c9-424c-8726-32bc01eb7e05","https://ldpdhalfeadjvie.weclapp.com/"] # doesn't work



	CredentialFields = ['key','url','account','username','email','password']

	# fill in this information based on the Credential Dictionary I'm building
	for i in range(0, len(CredentialDictionary[ConnectorName])):
		if CredentialDictionary[ConnectorName][i]:
			driver.find_element_by_id(CredentialFields[i]).send_keys(CredentialDictionary[ConnectorName][i])

	# click save
	driver.find_elements_by_class_name('ok')[1].click()

	if (ConnectorName == "MailChimp"):
		driver.find_elements_by_class_name('ok')[2].click()

def OffSiteAuth(driver,ConnectorName):

	# Constant Contact
	if (ConnectorName == "Constant Contact"):
		driver.find_element_by_id("luser").send_keys("bedrockdata")
		driver.find_element_by_id("lpass").send_keys("Spiffypants1")
		driver.find_element_by_name("_save").click()
		driver.find_element_by_name("authorize").click()

	if (ConnectorName == "Eventbrite"):
		driver.find_element_by_id("login-email").send_keys("devtools@bedrockdata.com")
		driver.find_element_by_id("login-password").send_keys("brd$123")
		driver.find_element_by_class_name("btn--epic").click()

	if ((ConnectorName == "GoToAssist") or (ConnectorName == "GoToTraining") or (ConnectorName == "GoToWebinar")):
		# if you're not on the GoTo site, you're auth'ed already
		time.sleep(1)
		try:
			driver.find_element_by_class_name('ok')
			driver.find_elements_by_class_name('ok')[1].click()
			return 1
		except NoSuchElementException:
			driver.find_element_by_id("emailAddress").send_keys("luke@bedrockdata.com")
			driver.find_element_by_id("password").send_keys("Spiffypants1")
			driver.find_element_by_id("submit").click()
			time.sleep(1)

	if (ConnectorName == "Nimble"):
		#oauth redirect issue on both qa0 and qa1
		driver.find_element_by_id("emailAddress").send_keys("devtools@bedrockdata.com")
		driver.find_element_by_id("emailAddress").send_keys(Keys.TAB)

		driver.find_element_by_id("password").send_keys("@scQU5w0m7VM")
		driver.find_element_by_name("login").click()


	if ((ConnectorName == "Salesforce.com") or (ConnectorName == "Veeva")):
		#oauth redirects to qa0
		driver.find_element_by_id("username").send_keys("devtools@bedrockdata.com")
		driver.find_element_by_id("password").send_keys("19%q&^b!ityH")
		driver.find_element_by_id("Login").click()

	if (ConnectorName == "Shopify"):
		driver.find_element_by_id("login-input").send_keys("adrian@bedrockdata.com")
		driver.find_element_by_id("password").send_keys("brd$123456")
		driver.find_element_by_name("commit").click()

		# the first time, you have to click again
		try:
			driver.find_elements_by_class_name("modal-open")
		except NoSuchElementException:
			time.sleep(1)
			driver.find_element_by_name("commit").click()
		
	# wait for page to load, click on Save
	Modal = WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.CLASS_NAME, "modal-open")))
	time.sleep(1) # extra insurance
	driver.find_elements_by_class_name('ok')[1].click()

def DynamicsAuth(driver):
	driver.find_element_by_name("settings[domain]").send_keys("https://crm2015.qa.bedrockdata.com:444")
	driver.find_element_by_name("settings[userid]").send_keys("bedrockdata-qa")
	driver.find_element_by_name("settings[password]").send_keys("bedrock123!")
	driver.find_element_by_name("settings[connection-type]").send_keys("Adfs")
	driver.find_element_by_name("settings[crm-domain]").send_keys("qa")

	driver.find_elements_by_class_name('ok')[1].click()

	time.sleep(3)


def AddConnectors(driver,ConnectorList):
	if (len(ConnectorList) == 0):
		return 1

	driver.find_element_by_class_name("addConnector").click()

	ConnectorName = ConnectorList[0]
	ConnectorSlug = ConnectorName.replace(".com","").replace(" ", "").replace(".","").replace("[","").replace("]","").lower()
	if (ConnectorSlug == "marketorest"):
		ConnectorSlug = "marketo_rest" 

	driver.find_elements_by_class_name("form-control")[0].send_keys(ConnectorName)
	driver.find_elements_by_class_name("form-control")[1].send_keys(ConnectorSlug)
	driver.find_element_by_id("state_public").click()

	driver.find_elements_by_class_name('btn-primary')[1].click()
	
	assertions.ConnectorAdded(driver,ConnectorName)
	ConnectorList.pop(0)
	time.sleep(1)
	if (len(ConnectorList) > 0):
		AddConnectors(driver,ConnectorList)
	return 1

def GoToConnectorsPage(driver):	

	connectors = WebDriverWait(driver, 10).until(
		EC.visibility_of_element_located((By.CLASS_NAME, "fa-cloud"))
	)
	connectors.click()

	time.sleep(2)


