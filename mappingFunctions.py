import time
import unittest
import sys

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import NoSuchElementException

# assumes user is logged in
def DeleteAndRegenerateMappings(driver):

	GoToMappingsPage(driver)

	dropDownList = driver.find_elements_by_class_name('dropdown-toggle')

	# loop through mappings and delete all the dropdowns beyond the first
	for x in range(1, len(dropDownList)):
		# you actually only need to hit the second one in the list every time
		driver.find_elements_by_class_name('dropdown-toggle')[1].click()

		DeleteButton = WebDriverWait(driver, 10).until(
			EC.element_to_be_clickable((By.LINK_TEXT, "Delete Mapping"))
		)
		DeleteButton.click()

		yeahDeleteIt = WebDriverWait(driver, 10).until(
			EC.element_to_be_clickable((By.CLASS_NAME, "btn-danger"))
		)
		yeahDeleteIt.click()

		# give it a second
		time.sleep(1)

	# auto-generate mappings
	driver.find_element_by_class_name('fa-magic').click()


def RefreshFieldInfo(driver):

	GoToMappingsPage(driver)

	refreshButton = WebDriverWait(driver, 10).until(
		EC.element_to_be_clickable((By.CLASS_NAME, "fa-refresh"))
	)
	refreshButton.click()


def AutoGenerateMappingsFromModal(driver):

	GoToMappingsPage(driver)

	driver.find_elements_by_class_name('fa-magic')[1].click()

	time.sleep(1)

def AutoGenerateMappings(driver):

	GoToMappingsPage(driver)

	driver.find_element_by_class_name('fa-magic').click()


def MakeCustomContactFields(driver,LongList,ShortList,NonHubspotType="Contact"):
	
	#wait for page to load, click on contact mappings
	contactMappings = WebDriverWait(driver, 10).until(
		EC.visibility_of_element_located((By.CLASS_NAME, "fa-user"))
	)
	contactMappings.click()

	for i in range(0,len(LongList)):

		# add a field, give it a label
		driver.find_element_by_class_name('add-field').click()
		driver.find_element_by_name('label').send_keys(LongList[i])

		#this makes the first field editable
		driver.find_element_by_class_name('value').click()

		driver.find_elements_by_tag_name('input')[3].click()
		driver.find_elements_by_tag_name('input')[3].send_keys(Keys.ENTER)

		driver.find_elements_by_tag_name('input')[4].click()
		driver.find_elements_by_tag_name('input')[4].send_keys(Keys.ENTER)

		driver.find_elements_by_tag_name('input')[5].send_keys(ShortList[i])
		driver.find_elements_by_tag_name('input')[5].send_keys(Keys.ENTER)

		# add another field
		driver.find_elements_by_class_name('fa-plus')[2].click()

		driver.find_elements_by_tag_name('input')[3].click()
		driver.find_elements_by_tag_name('input')[3].send_keys(Keys.ARROW_DOWN)
		driver.find_elements_by_tag_name('input')[3].send_keys(Keys.ENTER)

		driver.find_elements_by_tag_name('input')[4].send_keys(NonHubspotType)
		driver.find_elements_by_tag_name('input')[4].send_keys(Keys.ENTER)

		driver.find_elements_by_tag_name('input')[5].send_keys(ShortList[i])
		driver.find_elements_by_tag_name('input')[5].send_keys(Keys.ENTER)

		# save from this page
		driver.find_elements_by_class_name('btn-primary')[2].click()

		# this makes sense because it just has to get rid of the modal
		time.sleep(1)

def GoToMappingsPage(driver):

	mappings = WebDriverWait(driver, 10).until(
		EC.visibility_of_element_located((By.CLASS_NAME, "fa-exchange"))
	)
	mappings.click()

