import time
import datetime
import unittest
import sys
import accountFunctions
import mappingFunctions
import connectorFunctions
import assertions

from selenium import webdriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

def SignUpTest(devNum,IEVersion):

	IEVersion = str(IEVersion) + ".0"
	devNum = "389" + "-" + str(datetime.datetime.now().time())[-6:] + "-ie" + IEVersion
	desired_cap = {'browser': 'IE', 'browser_version': IEVersion, 'os': 'Windows', 'os_version': '7', 'resolution': '1024x768', 'acceptSslCerts': 'true','nativeEvents': 'false','unexpectedAlertBehavior': 'accept','ignoreProtectedModeSettings':'true','enablePersistentHover':'true','disable-popup-blocking':'true'}
	#URL = "http://bedrock.dev:8800/login/"
	URL = "https://qa1.bedrockdata.com/"
	InstallList = ["Batchbook","Capsule CRM"]

	driver = webdriver.Remote(
	    command_executor='http://johnmarcus:exAprfy5bqnyxP0NLdxs@hub.browserstack.com:80/wd/hub',
	    desired_capabilities=desired_cap)

	driver.get(URL)

	accountFunctions.CreateAccount(driver,devNum)

	connectorFunctions.InstallConnectors(driver,InstallList)

	mappingFunctions.AutoGenerateMappingsFromModal(driver)

	accountFunctions.DashboardSubscribe(driver)

	assertions.SubscribedOrNot(driver,"IE " + IEVersion)

	driver.quit()

for x in range (10,12):
	SignUpTest("389",x)
