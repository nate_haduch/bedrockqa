import time
import unittest
import sys
import accountFunctions
import connectorFunctions
import mappingFunctions
import assertions

from selenium import webdriver
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

driver = webdriver.Chrome()
credU = "nate@bedrockdata.com"
URL = "http://bedrock.dev:8800/"
URL = "https://accounts.qa0.bedrockdata.com/"
AddList = [""]
InstallList = ["HubSpot","Cvent"]
DevNum = sys.argv[1]

driver.get(URL)

accountFunctions.ChangeCustomer(driver,DevNum)

connectorFunctions.GoToConnectorsPage(driver)

connectorFunctions.AddConnectors(driver,AddList)

connectorFunctions.InstallConnectors(driver,InstallList)

mappingFunctions.AutoGenerateMappings(driver)

accountFunctions.SyncToggle(driver)