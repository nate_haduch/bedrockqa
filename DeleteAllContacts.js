var request = require('request');
var hapikey = "36e819ea-1fbf-44bd-b4e3-7ef4cc26cedd"

DeleteRecentCompanies();
Delete100Contacts();

function Delete100Contacts() {
  var getOptions = {
    uri: 'https://api.hubapi.com/contacts/v1/lists/all/contacts/all?hapikey=' + hapikey + "&propertyMode=value_only&count=100",
    method: 'GET',
    json: true,
    headers: {
            "Content-Type": "application/json"
     }
   };
  request(getOptions, function (error, response, body) {
    if (!error) { 
       for (var i=0;; i++) {
        if ( typeof body.contacts[i] == "undefined") {break;}; 
          console.log(body.contacts[i].vid)
          delURL = "https://api.hubapi.com/contacts/v1/contact/vid/" + body.contacts[i].vid + "?hapikey=" + hapikey
          request.del(delURL)
        }
    }
    else {
    	console.log(error)
    }
    Delete100Contacts();
  });
}

function DeleteRecentCompanies() {

    var getOptions = {
    uri: 'https://api.hubapi.com/companies/v2/companies/recent/created?hapikey=' + hapikey + "&count=200",
    method: 'GET',
    json: true,
    headers: {
            "Content-Type": "application/json"
     }
   };

  request(getOptions, function (error, response, body) {
    if (!error) { 
       for (var i=0;i<200; i++) {
        if ( typeof body.results[i].companyId == "undefined") {break;}; 
          console.log(body.results[i].companyId)
          delURL = "https://api.hubapi.com/companies/v2/companies/" + body.results[i].companyId + "?hapikey=" + hapikey
          request.del(delURL)
        }
    }
    else {
      console.log(error)
    }
  });

}