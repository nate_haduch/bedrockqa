import time
import unittest
import sys
import accountFunctions
import mappingFunctions

from selenium import webdriver
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

# constants to change as needed
URL = "http://my.qa1.bedrockdata.com/"
credU = "nate@bedrockdata.com"
LongList = ["Single Line Text 1", "Multi-Line Text 1","On/Off Checkbox 1","Number 1","Date Picker 1" ]
ShortList = ["Single", "Multi","On","Number","Date Picker"]
SecondType = "person" # usually Contact or Lead

# start chrome either way
driver = webdriver.Chrome()
driver.get(URL)

#create the account if it's new
if (len(sys.argv) > 2):
	if (sys.argv[2] == "new"):
		accountFunctions.CreateAccount(driver,sys.argv[1])
		HowManyMappings = 0

accountFunctions.Login(driver,credU)
accountFunctions.ChangeCustomer(driver,sys.argv[1])

mappingFunctions.DeleteAndRegenerateMappings(driver)
mappingFunctions.MakeCustomContactFields(driver,LongList,ShortList,SecondType)

time.sleep(5)

driver.close()
