import time
import unittest
import sys
import datetime

from termcolor import colored
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException
from selenium.common.exceptions import StaleElementReferenceException
from selenium.common.exceptions import NoSuchElementException


def SubscribedOrNot(driver,browserVersion):

	Dashboard = WebDriverWait(driver, 10).until(
		EC.visibility_of_element_located((By.CLASS_NAME, "fa-dashboard"))
	)

	Subscription = driver.find_elements_by_class_name("intro-header-primary")
	try:
		if (Subscription[2].text == "Subscription complete!"):
			print colored(u'\u2713' " Account subscribed successfully for " + browserVersion,"green")
	except TimeoutException:
		print colored("Didn't successfully subscribe for " + browserVersion,"red")

def InstallSuccess(ConnectorName,OldButton):
	try:
		if (OldButton.text == "INSTALL"):
			print colored(ConnectorName + " seems to not have installed or something.","red")
	except StaleElementReferenceException:
		print colored(u'\u2713' + " " + ConnectorName + " successfully installed.","green") 

def InstallFail(ConnectorName):
	try:
		Error = driver.find_element_by_class_name("alert-danger")
	except NoSuchElementException:
		print colored(ConnectorName + " was installed successfully, which in this case, is a failure.","red")
	else:
		print colored(u'\u2713' + " " + ConnectorName + " failed installation successfully.","green") 

def AccountCreated(driver,username):

	Dashboard = WebDriverWait(driver, 10).until(
		EC.visibility_of_element_located((By.CLASS_NAME, "fa-dashboard"))
	)
	try:
		WelcomeIntro = driver.find_element_by_class_name("secondary-text")
		print WelcomeIntro.text
		if (WelcomeIntro.text == "Install a connector below."):
			print colored(u'\u2713' + " " + username + " account created.","green")

	except NoSuchElementException:
		print colored( "Account was not created.","red") 

def ConnectorAdded(driver,ConnectorName):
	ConnectorNameList = driver.find_elements_by_tag_name("h4")
	if (ConnectorName in ConnectorNameList):
		print colored(u'\u2713' + " " + ConnectorName + " added successfully.","green") 
	else:
		print colored(ConnectorName + " seems to not have installed or something.","red")

def ConnectorNotSupported(ConnectorName):
	print colored(u'\u2713' + " " + ConnectorName + " is not supported for testing at this time.","red") 