var request = require('request');
var hapikey = "36e819ea-1fbf-44bd-b4e3-7ef4cc26cedd"
var date = new Date().toISOString()
var rands = date.substr(17,23)
date = date.replace(/\:/g,'').replace(/\-/g,'').replace(/\..+/, '')

var options = {
  uri: 'https://api.hubapi.com/contacts/v1/contact/?hapikey=' + hapikey,
  method: 'POST',
  json: true,
  headers: {
          "Content-Type": "application/json"
   },
  body:{
  		"properties": [
                {
                    "property": "email",
                    "value": "Nate" + date + "@example.com"
                }, 
                {
                    "property": "firstname",
                    "value": "Nade"
                },
                {
                    "property": "lastname",
                    "value": "Boggs" + rands
                },
                {
                    "property": "Single Line Text 1",
                    "value": "Single Line set in Hubspot API"
                },
                {
                    "property": "Number 1",
                    "value": 238
                },
                {
                    "property": "Multi-Line Text 1",
                    "value": "Multi-Line set in Hubspot API"
                }
             ]   
         }
};


request(options, function (error, response, body) {
  if (!error) {
    console.log(body) 
  }
  else {
  	console.log(error)
  }
});