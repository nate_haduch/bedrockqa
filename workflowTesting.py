import time
import unittest
import sys
import accountFunctions
import workflowFunctions
import connectorFunctions
import mappingFunctions

from selenium import webdriver
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

# URL = "https://my.qa1.bedrockdata.com"
URL = "http://bedrock.dev:8800/"
ConnectorList = ["Base CRM","Batchbook"]

# start chrome
driver = webdriver.Chrome()
driver.get(URL)

accountFunctions.CreateAccount(driver,sys.argv[1])

connectorFunctions.InstallConnectors(driver,ConnectorList)

mappingFunctions.AutoGenerateMappingsFromModal(driver)

workflowFunctions.GoToWorkflowPage(driver)
workflowFunctions.AddWorkFlow(driver)